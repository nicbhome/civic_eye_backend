<?php 

//$con = mysqli_connect("127.0.0.1","root","MySQL2018","public_eye");
$con = mysqli_connect("localhost","reelfishing","ReelFishing20","public_eye");
// Check connection
if (mysqli_connect_errno())
  {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

?>
<!DOCTYPE html>
<html>
<head>
<title>Civic Eye</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
body {
  background-color: whitesmoke;
}

/*@media only screen and (max-width: 600px) {
  body {
    #form{
      width: 100%
    }
  }
}*/
</style>
</head>
<body>
<center>
 <div class="card card-logo">
    <img src="assets/images/logo-v.png" alt="" class="logo_img" style="width:300px;margin-top:30px ">
  </div>
  <h2 style="color: #ffce07;">Reset Password</h2>
<?php

    $code =  $_REQUEST['code'];

    if($code == "")
    {
        echo "<h4 style='color:#da2d2c;margin:20px;'><b>Error: </b>Incorrect parameters.</h4>";
    }
    else
    {
        $check_query    = mysqli_query($con, "SELECT * FROM `restore_password_codes` WHERE `code` = '" . $code. "' and used is null");
        $check_data     = mysqli_fetch_assoc($check_query);

        if($check_data['expired_date'] == null)
        {
            echo "<h4 style='color:#da2d2c;margin:20px;'><b>Error: </b>Incorrect parameters. Please try again!</h4>";
        }
        else
        {
            $diff =  strtotime($check_data['expired_date']) - time();

            if($diff < 0)
            {
                echo "<h4 style='color:#da2d2c;margin:20px;'><b>Error: </b>Date expired.</h4>";
            }
            else
            {
                $new_password       =  $_REQUEST['new_password'];
                $confirm_password   =  $_REQUEST['confirm_password'];
                $display            =  0;

                if($new_password != "" && $confirm_password != "")
                {
                    if ($new_password != $confirm_password)
                    {
                        echo "<h4 style='color:#da2d2c;margin:20px;'><b>Error: </b>Passwords does not match</h4>";
                        $display = 1;
                    }
                    else
                    if (strlen($new_password) < 6)
                    {
                        echo "<h4 style='color:#da2d2c;margin:20px;'><b>Error: </b>Password has to be at least 6 characters long.</h4>";
                        $display = 1;
                    }
                    else
                    {
                        $new_password_enc = md5($new_password);
                        $user_id          = $check_data['user_id'];

                        $res = mysqli_query($con, "UPDATE `user` SET `password`='$new_password_enc' WHERE id=$user_id");

                        if($res == null)
                        {
                            echo "<h4 style='color:#da2d2c;margin:20px;'><b>Error: </b>Server error!</h4>";
                        }
                        else
                        {
                            echo "<h4 style='color:blue;margin:20px;'>Password has been changed successfully.</h4>";

                            $id     = $check_data['id'];
                            $date   = date( "Y-m-d H:i:s" );
                            $res    = mysqli_query($con, "UPDATE `restore_password_codes` SET `used`='$date' WHERE id=$id");
                        }
                    }
                }
                else
                {
                    $display = 1;
                }


                if($display) {
                    ?>

                    <form  method="post"
                          style="width:50%;border:1px solid #21274f;padding: 40px 0px 40px 0px;">
                        <div class="col-md-12 form-group">
                            <label for="email" style="float: left;color: #21274f;">New Password:</label>
                            <input type="hidden" class="form-control" id="code" name="code"
                                   value="<?php echo $code; ?>"/>
                            <input type="password" class="form-control" id="new_password" name="new_password"
                                   placeholder="Enter Password" required>
                        </div>
                        <div class="col-md-12 form-group">
                            <label for="pwd" style="float: left;color: #21274f;">Confirm Password:</label>
                            <input type="password" class="form-control" id="confirm_password" name="confirm_password"
                                   placeholder="Re-Enter password" required>
                        </div>
                        <button type="submit" class="btn btn-default" name="submit" style="font-family: Montserrat-Bold;
    font-size: 20px;line-height: 1.5;color: #e0e0e0;width: 50%;height: 50px;border-radius: 25px;
    background: #003278; ">Submit
                        </button>
                    </form>

                    <?php
                }
            }
        }
    }
  
?>

</center>
</body>
</html>