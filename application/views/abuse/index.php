
<div class="right_col" role="main">

    <div class='col-md-12 col-sm-12 col-xs-12 m-t-30 m-l-30 m-r-30'  > 
        <div class='title_text text-center ' style=" font-size: 16px; font-weight: 700;"> <?php echo $title; ?> </div>
        <div class='content_pannel pannel_bg m-t-20'  style="min-height:800px"  id="abuse_list">  </div>
    </div>
</div>

<script>
    $(".abuse").addClass("active");

    $(document).ready(function () {
          load_abuse_reports();
    });

   function load_abuse_reports(){
        $.ajax({
            type: "POST",
            data: {
            },
            url: "<?php echo base_url(); ?>abuse/load_abuse_reports",
            success: function(result) {
               $('#abuse_list').html(result);
             }
          });
    }

</script>


