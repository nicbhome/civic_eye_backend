<div class="right_col" role="main">

  <div class='col-md-12 col-sm-12 col-xs-12 m-t-30 m-l-30 m-r-30'  > 
        <div class='title_text text-center ' style=" font-size: 16px; font-weight: 700;"> <?php echo $title; ?> </div>
      
        <div class='content_pannel m-t-15'    style='display: flex'>
            <div style="width:89%;margin:auto;margin-left:0px" >
                <input class="form-control search_input" type="text" id="search_username" placeholder="Search">
            </div>
            <div style="width:10%;margin:auto;;margin-right:0px">
                <button class="form-control search_btn"  id="search_user">Search</button>
            </div>
        </div >

        <div class='content_pannel pannel_bg m-t-20' style="min-height:750px" id="user_list">
        </div>

  </div>   
    
</div>

<script>
    $(".banned").addClass("active");
    $(document).ready(function () {
          load_userlist(0,'');
    });

   function load_userlist(search_type,search_name){
        $.ajax({
            type: "POST",
            data: {
               banned_flag:1,  
               search_type: search_type,
               search_name: search_name
            },
            url: "<?php echo base_url(); ?>user/load_users",
            success: function(result) {
               $('#user_list').html(result);
             }
          });
    }

    $("#search_username").keypress(function( event ) {
          if ( event.which == 13 ) {
               event.preventDefault();
               console.log("press enter");
               $("#search_user").trigger('click');

          }
    });
   
    $(document).on('click', '#search_user', function (e) {
        e.preventDefault();
        search_name=$('#search_username').val();

        if(search_name.trim()!=''){
          search_name= search_name.trim()
          search_type=1;
       }else{
          search_type=0;
       }
       load_userlist(search_type,search_name);
    });


</script>


