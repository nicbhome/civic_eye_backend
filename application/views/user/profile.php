
<div class="right_col" role="main">

  <div class='col-md-12 col-sm-12 col-xs-12 m-t-30 m-l-30 m-r-30'  > 
        <div class='title_text text-center ' style=" font-size: 16px; font-weight: 700;"> <?php echo $title; ?> </div>
      
        <div class='content_pannel pannel_bg m-t-20' style="min-height:800px">
               
          <div class='row m-t-10 m-b-30' >
            <div class='text-center m-t-20 m-b-20'>
                  <input type="hidden" id="user_id" class="form-control" value="<?php echo $user_profile['id']; ?>">
                   <?php
                        if($user_profile['avatar']){
                    ?>
                       <img src="<?php echo $user_profile['avatar']; ?>" class="img-circle profile_avatar">

                    <?php
                         }
                         else{  
                    ?>
                       <img src="<?php echo base_url(); ?>assets/images/nouser.png" class="img-circle profile_avatar">
                    <?php
                         }
                    ?>
            </div>  
            <div class='text-center m-t-20 m-b-10'>
                <?php
                        if($user_profile['username']!=''){
                    ?>
                         <span  class='profile_name'> <?php echo $user_profile['username']; ?></span>

                    <?php
                         }else
                         {  
                    ?>
                         <span  class='profile_name'> <?php echo 'No NAMES'; ?></span>
                    <?php
                         }
                    ?>
           </div>  
           <div class='text-center m-t-20 m-b-10'>
                  <?php
                        if($user_profile['gender']!='' && $user_profile['age']!=null && $user_profile['age']!=0){
                    ?>
                        <span class='profile_age'><?php echo $user_profile['age'].','.$user_profile['gender']; ?> </span>
                    <?php
                         }else
                         {  
                    ?>
                         <span  class='profile_age'> <?php echo 'No Age, Gender'; ?></span>
                    <?php
                         }
                    ?>
           </div>  

           <div class='profile_btndiv text-center m-t-20 m-b-10' >
                        <?php
                            if($user_profile['banned_flag']==1){
                        ?>
                            <button class="form-control lift_btn"  id="lift_user_btn">Lift Ban</button>

                        <?php
                            }else
                            {  
                        ?>
                                <button class="form-control ban_btn"  id="ban_user_btn">Ban this user</button>
                        <?php
                            }
                        ?>
                </div>  
           </div>  
           <div class='user_report_list m-t-50'>
             <div class='user_report_list_title'>
                Reported Reports  
             </div>

             <?php   
            if ($user_reports) {
                foreach ($user_reports as $key => $report) {
            ?>

             <div class='report_item ' style="margin:0px">
                  <div class='user_report_item'>
                        <p >
                          <span class='report_label'> Crime Type:</span> <span class='report_comment m-r-50'><?php echo $report['report_type'] ?></span>  
                          <span class='report_label'> City:</span> <span class='report_city  m-r-50'> <?php echo $report['location']." , ".$report['city'] ?></span>  
                          <span class='report_label'> Report-Date:</span> <span class='report_comment'> <?php echo  date_format(date_create( $report['create_date']),'Y-m-d H:i') ?></span> 
                          <a href="<?php echo base_url(); ?>reports/report?id=<?php echo $report['id']?>" class='report_link_text'><?php echo "Report # ".$report['id'] ?></a>
                        </p>  
                 
                    </div>
              </div>
       
                <?php     
                    }
                }else{
                    ?>
                    <h4 class='text-center m-t-50'> There is no Reports.</h4>
                <?php
                    }     
                ?>
            <div>
          </div>
           
        </div>

  </div>   



</div>
<script>
  //  $(".allusers").addClass("active");

    $(document).on('click', '#ban_user_btn', function (e) {
        e.preventDefault();
        
        userid=$('#user_id').val();
        if(userid!=""){
            $.ajax({
            type: "POST",
            data: {
                userid: userid
            },
            url: "<?php echo base_url(); ?>user/ban_user",
            success: function(result) {
                if (result=="success"){
                //  console.log("tosast"+result);
                 // alert ('This user have banned.');

                 toastr.success('Banned Success', 'This user have been banned now.', {
                    "autoDismiss": false,
                    "positionClass": "toast-top-right",
                    "type": "success",
                    "timeOut": "2000",
                    "extendedTimeOut": "1000",
                    "allowHtml": false,
                    "closeButton": false,
                    "tapToDismiss": true,
                    "progressBar": false,
                    "newestOnTop": true,
                    "maxOpened": 0,
                    "preventDuplicates": false,
                    "preventOpenDuplicates": false
                    })

                    setTimeout(function(){  location.href = '../user'; }, 2100);          

                }
             }
          });
        }
    });


    $(document).on('click', '#lift_user_btn', function (e) {
        e.preventDefault();
        
        userid=$('#user_id').val();
        if(userid!=""){
            $.ajax({
            type: "POST",
            data: {
                userid: userid
            },
            url: "<?php echo base_url(); ?>user/lift_user",
            success: function(result) {
                if (result=="success"){
                //  console.log("tosast"+result);
                 // alert ('This user have banned.');

                 toastr.success('Lift Success', 'This user have been activate now.', {
                    "autoDismiss": false,
                    "positionClass": "toast-top-right",
                    "type": "success",
                    "timeOut": "2000",
                    "extendedTimeOut": "1000",
                    "allowHtml": false,
                    "closeButton": false,
                    "tapToDismiss": true,
                    "progressBar": false,
                    "newestOnTop": true,
                    "maxOpened": 0,
                    "preventDuplicates": false,
                    "preventOpenDuplicates": false
                    })

                    setTimeout(function(){  location.href = '../user/banned_users'; }, 2100);          

                }
             }
          });
        }
    });



    
</script>

<!-- from side div 2-->

