
<div class="right_col" role="main">

    <div class='col-md-12 col-sm-12 col-xs-12 m-t-30 m-l-30 m-r-30'  > 
        <div class='title_text text-center ' style=" font-size: 16px; font-weight: 700;"> <?php echo $title; ?> </div>
        <div class='content_pannel pannel_bg m-t-20'  style="min-height:800px" id="request_list">
        <?php   
        if ($requests) {
             $i=0;
             foreach ($requests as $key => $request) {
               
        ?>
                                          
          <div class='request_item m-b-20' >
               <div class='request_item_avatar '>

                    <a style='margin:auto' href="<?php echo base_url(); ?>user/profile?id=<?php echo $request['userid'] ?>" >
                  
                    <?php 
                         if($request['avatar']!=null && $request['avatar']!='' ){
                    ?>
                         <img src="<?php echo $request['avatar'] ?>" class='img-circle list_avatar' >
                    <?php 
                         }else{   
                    ?>
                         <img src="<?php echo base_url(); ?>assets/images/nouser.png" class='img-circle list_avatar'>
                    <?php 
                         }
                    ?>
                    </a>
               </div>  

               <div class='x_panel request_content '>
                    <div class='request_name'> 
                         <p style='width:99%' class="section_div"> 
                            <a  class='collapse-link' style="cursor:pointer"> 

                             <?php echo $request['username']." reported " ?> 
                                <span class="request_city" >
                                   <?php  echo $request['City']." ".$request['State'] ?>  
                                 </span>
                             <?php
                                   if ($request['fax']==1 && $request['email']==1){
                                        echo ' is missing Email/Fax#';
                                   }else{
                                        if ($request['fax']==1) {
                                             echo ' is missing Fax#';
                                        }elseif ($request['email']==1){
                                             echo ' is missing Email';
                                        }
                                   }
                             ?> 
                             </a>  
                          </p> 
                         <?php  if ($request['process_flag']==0) {?>
                              <a class="collapse-link fax_inputview_btn"> <i class="fa fa-chevron-up"></i> </a>
                              <?php
                                   }else{
                              ?>
                              <span  class="request_item_process"> Processed&nbsp;&nbsp;&nbsp;<a  href="<?php echo base_url(); ?>requests/del_request?id=<?php echo $request['id']; ?>" ><i class="glyphicon glyphicon-trash" style="color:red"></i></a> </span>
                              <?php
                                   }
                              ?>
                    </div>  
                    <div class='x_content'>
                         <div class='request_input_area'> 
                              <div class="request_input_city" >
                                   <?php  if ($request['fax']==1) {?>
                                        <input class="form-control main_input m-b-10" type="text" id="city_fax_<?php echo $i ?>" placeholder="City Fax Number#">
                                        <?php }
                                             if ($request['email']==1) {
                                        ?>
                                        <input class="form-control main_input" type="text" id="city_mail_<?php echo $i ?>" placeholder="City Email">
                                        <?php 
                                             }
                                        ?>
                              </div>
                         
                              <div class="request_fax_submitbtn">
                                   <button class="form-control search_btn" style="margin:auto" id="submit" onclick="process_request(<?php echo $i ?>)">Submit</button>
                              </div>
                         </div>
                    </div>  
               </div>
          </div>

          <?php     
               $i++;                          
               }
          }else{
               ?>
              <h2 class='text-center m-t-50'> There is no Email/Fax Requests.</h2>
          <?php
               }     
          ?>
          </div>
        
    </div>
</div>
<script>
    $(".requests").addClass("active");
    var requests;
    $(document).ready(function () {
     requests=JSON.parse('<?php echo json_encode($requests)?>');
        console.log(requests);
    });

    $(".section_div").on("click", function(e){
         
         if($(".fax_inputview_btn i", $(this).parent('.request_name')).hasClass('fa-chevron-down')){
           $(".fax_inputview_btn i", $(this).parent('.request_name')).removeClass('fa-chevron-down');
           $(".fax_inputview_btn i", $(this).parent('.request_name')).addClass('fa-chevron-up');
         }else{
               $(".fax_inputview_btn i", $(this).parent('.request_name')).removeClass('fa-chevron-up');
            $(".fax_inputview_btn i", $(this).parent('.request_name')).addClass('fa-chevron-down');
         }
           
    })
     function process_request(index){
              //   alert (requests[index]['City']);
          var city_email='';
          var city_email_input="#city_mail_"+index;
          var city_fax='';
          var city_fax_input="#city_fax_"+index;
          if (requests[index]['email']==1){
               city_email=$(city_email_input).val();
               if (city_email.trim()=='') return;

          }
          if (requests[index]['fax']==1){
               city_fax=$(city_fax_input).val();
               if (city_fax.trim()=='') return;

          }

         $.ajax({
            type: "POST",
            data: {
               request_id:requests[index]['id'],
               city_id:requests[index]['city_id'],
               user_email:requests[index]['user_email'],
               city_email:city_email,  
               city_fax: city_fax
            },
            url: "<?php echo base_url(); ?>requests/process_request",
            success: function(result) {
               if (result=='success'){
                    location.reload();
                   // alert ('ok');
               }
            }
          });
     }
</script>




