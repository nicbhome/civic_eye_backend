<!DOCTYPE html>
<html lang="en">
<head>
	<title>Civic Eye</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>/assets/images/icons/icon.png"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/vendor/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/login.css">
</head>

<body>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-t-10 p-b-30">
				<form class="login100-form validate-form">
					<div class="login100-form-logo m-b-25" style=" margin-bottom: 25px;">
						<img src="<?php echo base_url(); ?>/assets/images/logo-v.png" >
					</div>

					<div class="text-center w-full m-t-20 m-b-40" >
							<p style="color: black;font-weight: bold;"> Enter your mail and we'll help you <br/>
							reset your password </p>
					</div>

					<div class="wrap-input100 validate-input m-b-50" data-validate = "Email is required">
						<input class="input100" type="text" id="email" placeholder="Email">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope"></i>
						</span>
					</div>

					<div class="container-login100-form-btn p-t-10">
						<button class="login100-form-btn" id="reset_pass" >
							Reset Password
						</button>
					</div>

					<div class="text-center w-full p-t-25 p-b-30">
						<a href="../index.php" class="txt1">
							Sign In
						</a>
					</div>
					<!-- <div class="text-center w-full">
						<a class="txt1" href="#">
							Create new account
							<i class="fa fa-long-arrow-right"></i>
						</a>
					</div> -->
				</form>
			</div>
		</div>
	</div>

	<script src="<?php echo base_url(); ?>/assets/vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url(); ?>/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/vendor/select2/select2.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/main.js"></script>

	<script>
		$(document).ready(function () {
			$("#reset_pass").on("click", function (e) {
				e.preventDefault();

				email=$('#email').val();
				if (email.trim()!=''){
					$.ajax({
						type: "POST",
						url: "<?php echo base_url(); ?>login/reset_adminpass",
						data: {
							email: email
						},
						success: function (result) {

							if (result == 'success') {
								alert ('Reset Password sent to your email');
								 location.href = "<?php echo base_url(); ?>login";
							}
						}
					});
				}

			})
		});



	</script>
</body>
</html>
