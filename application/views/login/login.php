<!DOCTYPE html>
<html lang="en">
<head>
	<title>Civic Eye</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/images/icons/icon.png"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/login.css">
</head>
<body>


	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-t-10 p-b-30">
				<form class="login100-form validate-form"?>
					<div class="login100-form-logo m-b-70">
						<img src="<?php echo base_url(); ?>assets/images/logo-v.png" >
					</div>

					<div class="wrap-input100 validate-input m-b-20" data-validate = "Email is required">
						<input class="input100" type="text" id="email" placeholder="Email">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input m-b-10" data-validate = "Password is required">
						<input class="input100" type="password" id="pass" placeholder="Password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-key"></i>
						</span>
					</div>




					<div class="container-login100-form-btn p-t-10">
						<button class="login100-form-btn" id="submit">
							Log In
						</button>
					</div>

				</form>
			</div>
		</div>
	</div>




	<script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/vendor/select2/select2.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/main.js"></script>

	<script>
		$(document).ready(function () {
			$("#submit").on("click", function (e) {
				e.preventDefault();

				var email = $("#email").val();
				var password = $("#pass").val();

				//alert (email+ password);
				 //return;
                //alert ('index.php/Login/sign');

				if (email && password) {


                    //alert (base_url('index.php/Login/sign'));
                    //return;

                    //base_url('index.php/Login/sign')

					$.ajax({
						type: "POST",
						url: 'Login/sign',
						data: {
							email: email,
							password: password
						},
						dataType: "text",
						success: function (result) {

						    ///lert(result);
							if (result == 'true') {

								location.href = 'reports';
							} else {
								location.href = 'login';
							}
						}
					});
				}
			})
		});

	</script>
</body>
</html>
