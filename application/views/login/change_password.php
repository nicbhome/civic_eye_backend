
<div class="right_col" role="main">

<div class='col-md-12 col-sm-12 col-xs-12 m-t-30 m-l-30 m-r-30'  > 
        <div class='title_text text-center ' style=" font-size: 16px; font-weight: 700;"> <?php echo $title; ?> </div>
      
        <div class='content_pannel pannel_bg m-t-20' style="min-height:800px">
               
          <div class='row m-t-50 m-b-30' style="display:flex;" >
            <div class="col-sm-4 " style="margin:auto;">
                
                <label>Current Password</label>
                <div class="form-group pass_show"> 
                    <input type="password"  class="form-control main_input" placeholder="Current Password" id="old_pass"> 
                </div> 
                <label>New Password</label>
                <div class="form-group pass_show"> 
                    <input type="password"  class="form-control main_input" placeholder="New Password" id="new_pass"> 
                </div> 
                <label>Confirm Password</label>
                <div class="form-group pass_show"> 
                    <input type="password"  class="form-control main_input" placeholder="Confirm New Password" id="confirm_pass"> 
                </div> 
                
                <div class="form-group m-t-30"> 
                      <button class="form-control save_btn"  id="change_password">Save</button>
                </div> 
            </div>  

          </div>
           
        </div>

  </div>   
</div>

<script>

 $(".changepass").addClass("active");


$(document).ready(function(){
$('.pass_show').append('<span class="ptxt"><i class=" fa fa-eye	"></i></span>');  
});
  

$(document).on('click','.pass_show .ptxt', function(){ 

// $(this).text($(this).text() == "Show" ? "Hide" : "Show"); 

$(this).prev().attr('type', function(index, attr){return attr == 'password' ? 'text' : 'password'; }); 

});  


$(document).on('click', '#change_password', function (e) {
        e.preventDefault();

        old_pass=$('#old_pass').val();
        new_pass=$('#new_pass').val();
        confrim_pass=$('#confirm_pass').val();

        if(new_pass!=""  && old_pass!=''){

          if (new_pass==confrim_pass){
           // alert('ok');
              
              $.ajax({
              type: "POST",
              data: {
                old_pass: old_pass,
                new_pass: new_pass
              },
              url: "<?php echo base_url(); ?>login/update_adminpass",
              success: function(result) {
                  if (result=="success"){
                    toastr.success('Success', 'Admin password have been undate successfully.', {
                      "autoDismiss": false,
                      "positionClass": "toast-top-right",
                      "type": "success",
                      "timeOut": "2000",
                      "extendedTimeOut": "1000",
                      "allowHtml": false,
                      "closeButton": false,
                      "tapToDismiss": true,
                      "progressBar": false,
                      "newestOnTop": true,
                      "maxOpened": 0,
                      "preventDuplicates": false,
                      "preventOpenDuplicates": false
                      })
                  }else{
                    toastr.error('Error', 'Confirm old Password', {
                    "autoDismiss": false,
                    "positionClass": "toast-top-right",
                    "type": "success",
                    "timeOut": "2000",
                    "extendedTimeOut": "1000",
                    "allowHtml": false,
                    "closeButton": false,
                    "tapToDismiss": true,
                    "progressBar": false,
                    "newestOnTop": true,
                    "maxOpened": 0,
                    "preventDuplicates": false,
                    "preventOpenDuplicates": false
                    })

                  }
              }
            });
          }else{
            toastr.error('Error', 'Confirm New Password', {
                    "autoDismiss": false,
                    "positionClass": "toast-top-right",
                    "type": "success",
                    "timeOut": "2000",
                    "extendedTimeOut": "1000",
                    "allowHtml": false,
                    "closeButton": false,
                    "tapToDismiss": true,
                    "progressBar": false,
                    "newestOnTop": true,
                    "maxOpened": 0,
                    "preventDuplicates": false,
                    "preventOpenDuplicates": false
                    })
          }
        }else{
               toastr.error('Error', 'Input Password fields.', {
                    "autoDismiss": false,
                    "positionClass": "toast-top-right",
                    "type": "success",
                    "timeOut": "2000",
                    "extendedTimeOut": "1000",
                    "allowHtml": false,
                    "closeButton": false,
                    "tapToDismiss": true,
                    "progressBar": false,
                    "newestOnTop": true,
                    "maxOpened": 0,
                    "preventDuplicates": false,
                    "preventOpenDuplicates": false
                    })
        }
    });

</script>
<!-- from side div 2-->

