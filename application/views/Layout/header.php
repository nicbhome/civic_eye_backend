<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $title; ?></title>
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/images/icons/icon.png"/>
    <link  href="<?php echo base_url(); ?>assets/vendor/bootstrap1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link  href="<?php echo base_url(); ?>assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link  href="<?php echo base_url(); ?>assets/css/custom.min.css" rel="stylesheet">
    <link  href="<?php echo base_url(); ?>assets/css/main.css" rel="stylesheet">
    <link  href="<?php echo base_url(); ?>/assets/css/util.css" rel="stylesheet" type="text/css">
    <link  href="<?php echo base_url(); ?>/assets/css/lightslider.min.css" rel="stylesheet" type="text/css">

  </head>
  <script src="<?php echo base_url(); ?>assets/vendor/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/bootstrap1/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/toastr.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lightslider.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/custom.min.js"></script>

  <body class="nav-md" style="background:#F7F7F7">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col " style="position:fixed">
          <div class="left_col scroll-view left_side_bg side_left_border" >
            <div class="navbar nav_title left_side_bg section_border" style="border: 0;">
             <img class="side_logo"  src="<?php echo base_url(); ?>assets/images/logo-h.png" >

            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <!-- div class="profile clearfix  section_border">
              <div class="profile_pic">
                <img src="<?php echo base_url(); ?>assets/images/admin-user.png" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info  m-b-15">
                <span class="side_textcolor">Welcome,</span>

                  //store-baptiste-bryant

                <h2 class="side_textcolor">Shawn Williams</h2>
              </div>
            </div -->
            <!-- /menu profile quick info -->

                  <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <ul class="nav side-menu"  style="margin-top:0px">
                  <li class='side_menu_item reports'><a class="menu_item_a" href="<?php echo base_url(); ?>reports"><i class="fa fa-file-word-o"></i> <span>Reports </span></a>

                  </li>
                  <li class='side_menu_item abuse'><a class="menu_item_a" href="<?php echo base_url(); ?>abuse"> <i class="fa fa-flag"></i> <span>Abuse</span></a>

                  </li>

                  <li class='side_menu_item allusers'> <a class="menu_item_a" href="<?php echo base_url(); ?>user"><i class="fa fa-users"></i>  <span>All Users</span></a>

                  </li>
                  <li class='side_menu_item banned'> <a class="menu_item_a" href="<?php echo base_url(); ?>user/banned_users"><i class="fa fa-user-times"></i>  <span>Banned Users</span></a>

                  </li>
                  <li class='side_menu_item changepass'><a class="menu_item_a" href="<?php echo base_url(); ?>login/change_password" ><i class="fa fa-lock"></i>  <span>Change Password</span> </a>

                  </li>
                  <li class='side_menu_item requests'><a class="menu_item_a" href="<?php echo base_url(); ?>requests"><i class="fa fa-hashtag"></i>  <span>Fax/Email Requests </span></a>

                  </li>

                </ul>
              </div>


            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small " style=' background: #e6e9ed!important;'>
              <a class='logout_a' data-toggle="tooltip" data-placement="top" title="Logout" id="logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"> </span>
                &nbsp;&nbsp; Logout
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>
