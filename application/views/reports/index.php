
<div class="right_col" role="main">
    <div class='col-md-12 col-sm-12 col-xs-12 m-t-30 m-l-30 m-r-30'  > 
        <div class='title_text text-center ' style=" font-size: 16px; font-weight: 700;"> <?php echo $title; ?> </div>
        <div class='content_pannel pannel_bg m-t-20'  style="min-height:800px" id="report_list">
        </div>
    </div>
</div>

<script>
    $(".reports").addClass("active");
    $(document).ready(function () {
          load_reports();
    });

   function load_reports(){
        $.ajax({
            type: "POST",
            data: {
            },
            url: "<?php echo base_url(); ?>reports/load_reports",
            success: function(result) {
               $('#report_list').html(result);
             }
          });
    }
</script>
