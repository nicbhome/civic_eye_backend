
<div class="right_col" role="main">

  <div class='col-md-12 col-sm-12 col-xs-12 m-t-30 m-l-30 m-r-30'  > 
        <div class='title_text text-center ' style=" font-size: 16px; font-weight: 700;"> <?php echo $title; ?> </div>
      
        <div class='content_pannel pannel_bg m-t-20' style="min-height:800px">
               
           <div class='report_detail_item m-t-10 m-b-10' >
                  <div class='user_item_avatar'>
                      <input type="hidden" id="report_id" class="form-control" value="<?php echo $report['id']; ?>">

                       <?php
                        if($report['avatar']){
                         ?>
                         <img src="<?php echo $report['avatar']; ?>" class="img-circle list_avatar">

                         <?php
                              }
                              else{  
                         ?>
                         <img src="<?php echo base_url(); ?>assets/images/nouser.png" class="img-circle list_avatar">
                         <?php
                              }
                         ?>

                  </div>  
                  <div class='report_info1'>
                       <p style='margin-top:5px'><span class='report_label'> Reporter:</span> 
                        <span class='report_comment m-r-50'>
                          <?php 
                              if ($report['username']!='')
                                  echo $report['username']." (" .$report['email'].")" ;
                              else
                                  echo $report['email'] ;                
                         ?>  
                         </span> 
                       </p>  
                       <p>  <span class='report_label'> Crime Type:</span> <span class='report_comment m-r-50'><?php echo $report['report_type']; ?> </span> </p>
                   </div>
                  <div class='report_del'>
                      <button class="form-control del_report_btn"  id="del_btn" onclick="delete_post();">Delete Report</button>
                  </div>
             </div>

             <div class='report_detail_item m-t-10 m-b-10 p-l-20' style='height:60px' >
            
                  <div class='report_info2'>
                      <p> <span class='report_city  m-r-50'> <?php echo $report['location']." , ".$report['city'] ?></span> </p>
                      <p> <span class='report_comment'><?php echo date_format(date_create( $report['create_date']),'Y-m-d H:i') ?><span></p>
                   </div>
              </div>
              <div class='report_detail_item description_div m-t-10 p-l-20' >
                 <span class="report_description" >  <?php echo $report['description'] ?> </span>
              </div>

              <div class='report_detail_media' >
                 <div  class="report_slider_div" >

                    <ul id="lightSlider" style="height:100%!important">
                      <?php if($report['photo1']!=''){  ?>
                         <li >
                              <div  class='slider_item'  >
                                   <img src="<?php echo $report['photo1'] ?>"   class="slide_photo" />
                              </div>
                         </li>
                      <?php   }     ?>
                      <?php if($report['photo2']!=''){  ?>
                        <li >
                              <div  class='slider_item'  >
                                   <img src="<?php echo $report['photo2'] ?>"  class="slide_photo" />
                              </div>
                         </li>
                      <?php   }     ?>
                      <?php if($report['photo3']!=''){  ?>
                         <li >
                              <div class='slider_item'>
                                 <img src="<?php echo $report['photo3'] ?>"  class="slide_photo" />
                              </div>

                         </li>
                      <?php   }     ?>
                      <?php if($report['video']!=''){  ?>
                         <li >
                              <div class='slider_item'>

                                   <video class="slide_video"  controls  >
                                        <source src="<?php echo $report['video'] ?>" type="video/mp4">
                                        Your browser does not support the video tag.
                                   </video>
                              </div>

                         </li>
                      <?php   }     ?>
                      <?php if($report['audio']!=''){  ?>
                         <li >
                              <div class='slider_item'>
                                   <audio class="slide_audio" controls style="margin:auto;">
                                        <source src="<?php echo $report['audio'] ?>" type="audio/mpeg">
                                        Your browser does not support the audio element.
                                        </audio>                            
                               </div>

                         </li>
                      <?php   }     ?>
                    </ul>

                 </div>
               </div>
        </div>
  </div>   
</div>
<script>
    /*
   $(document).ready(function() {
     $('#lightSlider').lightSlider({
          item: 1,
          loop: true,
          // slideMove: 3,
          // auto: true,
          addClass: '',
          mode: "slide",
          useCSS: true,
          cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
          speed: 1000,
          easing: 'linear',
          slideMargin: 20,
          controls: false,
          responsive: [{
                    breakpoint: 3000,
                    settings: {
                         item: 1,
                         slideMove: 1,
                         slideMargin: 20,
                    }
               },
               {
                    breakpoint: 991,
                    settings: {
                         item: 1,
                         slideMove: 1,
                         slideMargin: 20
                    }
               },
               {
                    breakpoint: 768,
                    settings: {
                         item: 1,
                         slideMove: 1,
                         slideMargin: 20
                    }
               }
          ]
       });

 
       
     });
     */
    
   function delete_post(){
        
     var report_id = $("#report_id").val();
     $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>reports/delete_report",
          data: {
               report_id: report_id
          },
          success: function (result) {
               if (result == 'success') {
                   location.href = '<?php echo base_url(); ?>reports';
               } 
          }
     });
   }

</script>

<!-- from side div 2-->

