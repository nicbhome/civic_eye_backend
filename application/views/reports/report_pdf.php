<!DOCTYPE html>
<html lang="en">
<head>
  <title> Civic Eye Report  <?php echo $report_id; ?>
   </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body style="padding: 30px;padding-top:50px;">
    <h3 style="text-align: center"> Civic Eye - Report </h3>
    <br/><br/>
    <P> Good day! </P>
    <P> Civic Eye is a mobile app which allows citizens a means to report any government official involvement in malpractice, abuse of power, and harassment.  </P>
    <P> A detailed report may provide detailed information, scene descriptions, photos, and videos pertaining to a crime. </P>
    <P> A report concerning a government official under your jurisdiction was sent via our app.  </P><br/>
    <P>You may view the details of this report by downloading our app from Google Play Store <a href='<?php echo $app_url; ?>'> Civic eye App  - <?php echo $app_url; ?> </a> </P>
    <P>REPORT NUMBER:  <?php echo $report_id; ?> </P><br/><br/><br/><br/>

    <P style="text-align: center;font-size:20px;font-weight:700">Steps on how to retrieve the report: </P><br/>
    <P> 1. Download our app from Google Play Store : <a href='<?php echo $app_url; ?>'> Civic eye App - <?php echo $app_url; ?> </a> and sign up </P>
    <P> 2. Click on # icon and type the report number on the Enter Report Number field then click Search </P>
    <P> 3. Report details should appear on the screen </P><br/>
    <P> Civic Eye app aims to help government combat corruption and malpractice by giving citizens the means to report their concerns directly to the mayor’s office. </P>
    <P> If you have any concerns regarding this report, you may email the app administrators through <?php echo $admin_mail; ?> </P>
    <P style="text-align: right"> Best regards, Civic Eye Team  </P><br/>

</body>
</html>
