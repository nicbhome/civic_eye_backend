<?php

require "vendor/autoload.php";

use PHPMailer\PHPMailer\PHPMailer;

defined('BASEPATH') or exit('No direct script access allowed');
// require APPPATH . 'libraries/REST_Controller.php';
          
class App_api extends CI_Controller {
    
    /**
   * Get All Data from this method.
   *
   * @return Response
  */
    public function __construct() {
     parent::__construct();
     $this->load->helper('url');
     $this->load->helper('mailgun');
     $this->load->helper('fax');

     $this->load->model('User_model');
     $this->load->model('Report_model');
     $this->load->library('pdf');

    }

    public function login(){
        $email=$this->input->post('email');
        $password=md5($this->input->post('password'));

        if (!empty($email) && !empty($password)){
            $user = $this->User_model->loginuser($email,$password);

            if ($user){
                if ($user['banned_flag']==0){
                    $userid=$user['id'];

                    $user = $this->User_model->get_userprofile($userid);
                    $report= $this->Report_model->get_user_report($userid);
                    $city_list = $this->Report_model->get_all_citydata();

                    $data['status']='OK';
                    $data['msg']='Login Success';
                    $data['userid']=$user['id'];
                    $data['username']=$user['username'];
                    $data['gender']=$user['gender'];
                    $data['age']=$user['age'];
                    $data['avatar']=$user['avatar'];
                    $data['reported_reports']=$report;
                    $data['city_list']= $city_list;

                }else{
                    $data['status']='Banned';
                    $data['msg']='Your account is banned';
                }               
            }else{
                $data['status']='Error';
                $data['msg']='Invalid email and/or password';
            }

        }else{
            $data['status']='Input error';
            $data['msg']='Input your email and password';
        }
        echo json_encode($data);
    }

    public function signup(){
        $email=$this->input->post('email');
        $password=md5($this->input->post('password'));

        if (!empty($email) && !empty($password)){
            $user = $this->User_model->registeruser($email,$password);

            if ($user){
                    $data['status']='OK';
                    $data['msg']='You have registered successfully.';
                
            }else{
                $data['status']='Error';
                $data['msg']='You have failed on signup';
            }

        }else{
            $data['status']='Input error';
            $data['msg']='Input your email and password';
        }
        echo json_encode($data);
    }

    public function  get_all_useremail(){
        $user = $this->User_model->get_all_useremail();
        
        if ($user){
            $data['status']='OK';
            $data['user_email_array']= $user ;
        }else{
            $data['status']='Error';
            $data['msg']='There is no error on getting emails';
        }
        echo json_encode($data);

    }


    public function confirm_email(){
        $email=$this->input->post('email');

        if (!empty($email)){
            $user = $this->User_model->confirm_email($email);
            if ($user){
                $data['status']='Error';
                $data['msg']='This Email is used already';
      
            }else{
                $data['status']='OK';
                $data['msg']='New Email';
            }

        }else{
            $data['status']='Input error';
            $data['msg']='Input your email!';
        }
        echo json_encode($data);
    }

    public function change_password(){
        $userid=$this->input->post('userid');
        // $oldpass=$this->input->post('oldpassword');
        $newpass=$this->input->post('newpassword');

        if (!empty($userid) && !empty($newpass) ){
            $update_pass = $this->User_model->change_password($userid,md5($newpass));
                if ($update_pass){
                    $data['status']='OK';
                    $data['msg']='Your password is updated successfully.';
                } else{
                    $data['status']='Error';
                    $data['msg']='Your password updating failed.';

                }             
        }else{
            $data['status']='Input error';
            $data['msg']='Input your userid';
        }
        echo json_encode($data);
    }

    public function reset_password(){
        $email=$this->input->post('email');
        //$ars = explode('@', $email);

        //$email="nicb.home@gmail.com";

        //print_r($ars);die;

        if (!empty($email)){
            $user       = $this->User_model->confirm_email($email);

            if ($user){

                $code           = uniqid('', false);

                $this->User_model->regCode($user['id'], $code);

                $link = "http://" . $_SERVER['HTTP_HOST'] . "/civic_eye/reset.php?code=" . $code;

                //print ( $link );

                //9
                //Civiceyeapp@gmail.com password is: Alleyes88$

                $mail               = new PHPMailer(true); // create a new object

                //try {

                $mail->IsSMTP();
                $mail->SMTPDebug    = 0; // debugging: 1 = errors and messages, 2 = messages only
                $mail->SMTPAuth     = true; // authentication enabled
                $mail->SMTPSecure   = 'ssl'; // secure transfer enabled REQUIRED for Gmail
                $mail->Host         = "smtp.gmail.com";
                $mail->Port         = 465; // or 587
                $mail->IsHTML(true);

                $mail->Username     = "civiceyeapp@gmail.com";
                $mail->Password     = "Alleyes88$";

                //$mail->Username     = "nicb.home@gmail.com";
                //$mail->Password     = "Angel1977!";

                //print "AA";


                $mail->SetFrom("civiceyeapp@gmail.com");
                $mail->Subject      = "Reset Password";

                $mail->AddAddress($email);

                $mail->Body ='<!DOCTYPE html>
                     <html>
                   <head></head>
                 <body style=" margin: 0 auto;">
                 <div class="wrapper" style="width:100%;" > 
                    <header style="width: 100%;float: left;clear: left;text-align: center;">                     
                	                      
                    </header>
                    <section>
                       <div class="container" style="width: 100%; margin: 0 auto;overflow: hidden; max-width: 1170px;">
                         <div class="section">                   
                           <p style="font-size:14px;">Hello, </p>
                           <p >We got a request to reset your password.</p>
                           <p>Click on this link to reset your password  :  <span><a href="' . $link .'" >' . $link . '</a></span></p>                          
                           <p >If you ignore this email your password won\'t be changed.</p>
                           <p >If you did not request a password reset <a> let us know</a></p>
                           </br>
                           </br>
                           </br>
                           </br>
                           </br>
                           </br>
                           <p style="font-size:18px;margin-top:9px;">Civic Eye</p>
                         </div>
                       </div>
                     </section>
                 <footer style="  color: white; background-color: black; height: auto; width: 100%; float: left;">
                 <div class="container"  style="width: 100%; margin: 0 auto;overflow: hidden; max-width: 1170px;">
                   <div class="main-box" style=" width: 100%;">
                     </div>
                 </footer>
                 </div>
                 </body>
                 </html>';

                /*
                try {
                    $mail->Send();
                }
                catch (phpmailerException $e)
                {
                    echo $e->errorMessage();
                }
                catch (Exception $e) {
                    echo $e->getMessage(); //Boring error messages from anything else!
                }
*/

                if(!$mail->Send())
                {
                    $data['status']='Failed';
                    $data['msg']='You  failed to reset your password';
                }
                else {
                    $data['status'] = 'OK';
                    $data['msg'] = 'Send a reset password to your Email';
                }

                }else{
                    $data['status']='Error';
                    $data['msg']='Your email is not registered';
                }

        }else{
            $data['status']='Input error';
            $data['msg']='Input your email!';
        }
        echo json_encode($data);
    }

    public function reset_password_old(){

        /*
        $email=$this->input->post('email');

        $pass=rand();

        if (!empty($email)){
            $user = $this->User_model->confirm_email($email);

            if ($user){

                $to=$email;
                $subject="Civic Eye App- RESET PASSWORD";
                $message='';
                $message=$message.'From: Civic Eye Support <br/> <br/>';
                $message=$message.'Your password is reset:'.$pass.'<br/>';
              
                $mail_header="MIME-Version: 1.0 ";
                $mail_header .= "From: Civic Support \r\n";
                $mail_header .= "Content-type: text/html;charset=utf-8 ";
                $mail_header .= "X-Priority: 3";
                $mail_header .= "X-Mailer: smail-PHP ".phpversion();

            //    $retval = mail($to, $subject, $message, $mail_header);
                $retval= sendmailbymailgun($to, "Civic Eye App User", get_adminmail(), "Civic Eye Support", $subject, $message );
                $retval1= mail($to, $subject, $message, $mail_header);
                
                if( $retval == "true" ||  $retval1==true ) {
                    $update_pass = $this->User_model->reset_password($email,md5($pass));
                    if ($update_pass){
                        $data['status']='OK';
                        $data['msg']='Send a reset password to your Email';
                    } else{
                        $data['status']='Failed';
                        $data['msg']='You failed to reset your password';
                    }             
         
                }else {
                    $data['status']='Failed';
                    $data['msg']='You  failed to reset your password';
                }
         
            }else{
          
                $data['status']='Error';
                $data['msg']='Your email is not registered';
            }

        }else{
            $data['status']='Input error';
            $data['msg']='Input your email!';
        }
        echo json_encode($data);
        */
    }

    public function get_userprofile(){
        $userid=$this->input->post('userid');
        if (!empty($userid)){
            $user = $this->User_model->get_userprofile($userid);
            $report= $this->Report_model->get_user_report($userid);

            if ($user){
                $data['status']='OK';
                $data['msg']='You have your profile successfully.';
                $data['username']=$user['username'];
                $data['gender']=$user['gender'];
                $data['age']=$user['age'];
                $data['avatar']=$user['avatar'];
                $data['reported_reports']=$report;

            }else{
                $data['status']='Error';
                $data['msg']='You failed to get your profile.';
     
            }
        }else{
            $data['status']='Input error';
            $data['msg']='Input your userid!';
        }
        echo json_encode($data);
    }

    public function update_username(){
        $userid=$this->input->post('userid');
        $username=$this->input->post('username');

        if (!empty($userid) && !empty($username)){
            $user = $this->User_model->update_username($userid,$username);

            if ($user){
                    $data['status']='OK';
                    $data['msg']='Your username is updated successfully.';
            }else{
                $data['status']='Error';
                $data['msg']='Updating username failed';
            }

        }else{
            $data['status']='Input error';
            $data['msg']='Input your userid and username';
        }
        echo json_encode($data);
    }

    public function update_gender_age(){
        $userid=$this->input->post('userid');
        $gender=$this->input->post('gender');
        $age=$this->input->post('age');

        if (!empty($userid) && !empty($gender) && !empty($age)){
            $user = $this->User_model->update_gender_age($userid,$gender,$age);

            if ($user){
                    $data['status']='OK';
                    $data['msg']='Your gender and age are updated successfully.';
            }else{
                $data['status']='Error';
                $data['msg']='User gender and age updating failed.';
            }

        }else{
            $data['status']='Input error';
            $data['msg']='Input your gender and age';
        }
        echo json_encode($data);
    }

    
    public function update_avatar()
    {
        $userid=$this->input->post('userid');
        if (isset($_FILES['avatarfile']) && !empty($userid)){
            $config['upload_path'] = './uploads/useravatar';
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            // $config['max_size'] = 10000;
            // $config['max_width'] = 3300;
            // $config['max_height'] = 3024;
            $config['overwrite'] = true;

      
            $new_name = "useravatar_".$userid.'_'.$_FILES['avatarfile']['name'];
            $config['file_name'] = $new_name;
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('avatarfile')) {
             
                $avatarfile=$this->upload->data();
                //$avatar_url=$this->config->base_url().'uploads/useravatar/'.$avatarfile['file_name'] ;
                $avatar_url=$this->config->base_url().'uploads/useravatar/'.$avatarfile['file_name'] ;
              
                $avata_update=$this->User_model->update_avatar($userid,$avatar_url);

                if($avata_update){
                    $data['status']='OK';
                    $data['msg']='Your profile image have updated successfully.';
                    $data['avatar']=$avatar_url;

                }else{
                    $data['status']='Error';
                    $data['msg']='User avatar updating failed.';
                    $data['avatar']=$avatar_url;
                }
            } else {
                $data['status']='Error';
                $data['msg']='User avatar uploading failed.';
            }
        }else{
            $data['status']='Input error';
            $data['msg']='Input your avatar file';
        }
        echo json_encode($data);
    }



    public function send_report(){
         //static_const
         $APP_URL="https://play.google.com/store/apps/details?id=com.auctions.com";
         $ADMIN_MAIL= get_adminmail();   

      //   $report_id= $this->Report_model->get_MaxReportid() +1;

         $all_report_ids = $this->Report_model->get_all_reportid();
         do {
          $report_id=rand(10000001,99999999);
          $result= array_search($report_id,$all_report_ids);
         } while ($result==true);
    
         $parm['id']=$report_id;
         $parm['userid']=$this->input->post('userid');
         $parm['report_type']=$this->input->post('report_type');
         $parm['officer_Name']=$this->input->post('officer_Name');
         $parm['officer_CarId']=$this->input->post('officer_CarId');
         $parm['officer_BadgeNumber']=$this->input->post('officer_BadgeNumber');
         $parm['officer_CollarBrass']=$this->input->post('officer_CollarBrass');
         $parm['officer_PrecintDepartment']=$this->input->post('officer_PrecintDepartment');
         $parm['officer_MaleOrFemale']=$this->input->post('officer_MaleOrFemale');
         $parm['officer_additional']=$this->input->post('officer_additional');
         $parm['description']=$this->input->post('description');
         $parm['location']=$this->input->post('location');
         $parm['city']=$this->input->post('city');
         $parm['reporter_type']=$this->input->post('reporter_type');
         $parm['anonymous_flag']=$this->input->post('anonymous_flag');
         $parm['receive_city_id']=$this->input->post('receive_city_id');
         $parm['receive_fax']=$this->input->post('receive_fax');
         $parm['receive_email']=$this->input->post('receive_email');
         $parm['create_date']= date('Y-m-d H:i:s');


         //print_r($_FILES);



        if (!empty($parm['userid']) && !empty( $parm['report_type']) && !empty($parm['description'])  && !empty($parm['receive_city_id']) ){
        

         ///////////////  file Upload report attach///////////////

        /// create directory if not exist directory
            $upload_dir = "./uploads/".date('Y')."/".date('m')."/";
            if (!is_dir($upload_dir)) {
                mkdir($upload_dir, 0777, TRUE);
            }

            $config['upload_path'] =  $upload_dir;
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['overwrite'] = true;

            if (isset($_FILES['photo1'])){
                $config['file_name'] = "reports_".$report_id.'_p1_'.$_FILES['photo1']['name'];
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('photo1')){
                    $photo1=$this->upload->data();
                    $parm['photo1']=$this->config->base_url().$upload_dir.$photo1['file_name'] ;    
               
                }
            }else{
                $parm['photo1']='';
            }
        
        
            if (isset($_FILES['photo2'])){
     
                $config['file_name'] = "reports_".$report_id.'_p2_'.$_FILES['photo2']['name'];
                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if($this->upload->do_upload('photo2')){
                    $photo2=$this->upload->data();
                    $parm['photo2']=$this->config->base_url().$upload_dir.$photo2['file_name'] ;
                }
            }else{
                $parm['photo2']='';
            }
    
            if (isset($_FILES['photo3'])){
       
                $config['file_name'] = "reports_".$report_id.'_p3_'.$_FILES['photo3']['name'];
                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if($this->upload->do_upload('photo3')){
                    $photo3=$this->upload->data();
                    $parm['photo3']=$this->config->base_url().$upload_dir.$photo3['file_name'] ;
                }
            }else{
                $parm['photo3']='';
            }

            if (isset($_FILES['video'])){
                $config['allowed_types'] = '*';
                $config['file_name'] = "reports_".$report_id.'_v_'.$_FILES['video']['name'];
                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if($this->upload->do_upload('video')){
                    $video=$this->upload->data();
                    $parm['video']=$this->config->base_url().$upload_dir.$video['file_name'] ;
                }
                else
                {
                    $data['status']='Input error';
                    $data['msg']=$this->upload->display_errors();
                    echo json_encode($data);
                    exit();
                }

            }else{
                $parm['video']='';
            }
            
            if (isset($_FILES['audio'])){
                $config['allowed_types'] = '*';
                $config['file_name'] = "reports_".$report_id.'_m_'.$_FILES['audio']['name'];
                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if($this->upload->do_upload('audio')){
                    $audio=$this->upload->data();
                    $parm['audio']=$this->config->base_url().$upload_dir.$audio['file_name'] ;
                }
            }else{
                $parm['audio']='';
            }
           //////////////-------------------------------/////////////////////////        
                         
           
            $report_insert_id= $this->Report_model->save_report($parm);
            $report_insert_id=  $report_id;

          /////////////////////////  send email,fax //////////////////////////////

            $receive_city= $this->Report_model->get_cityinfo($parm['receive_city_id']);
            $report_user=$this->User_model->get_userprofile($parm['userid']);

            $mailto=$receive_city['Email'];
            $faxto=$receive_city['Fax'];

            $subject="Report to " .$receive_city['City'] ." , " .$receive_city['Email']."  ".$receive_city['Title'];
            $message='';
            $message=$message."<P> Good day! </P>";
            $message=$message."<P> Civic Eye is a mobile app which allows citizens a means to report any government official involvement in malpractice, abuse of power, and harassment.  </P>";
            $message=$message."<P> A detailed report may provide detailed information, scene descriptions, photos, and videos pertaining to a crime. </P>";
            $message=$message."<P> A report concerning a government official under your jurisdiction was sent via our app.  </P><br/>";
            $message=$message."<P>You may view the details of this report by downloading our app from Google Play Store <a href='" . $APP_URL ."'> Civic eye App </a> </P>";

            $message=$message."<P>REPORT NUMBER: ".$report_insert_id  ." </P><br/>";
            $message=$message."<P>Steps on how to retrieve the report: </P>";
            $message=$message."<P> 1. Download our app from Google Play Store : <a href='".$APP_URL. "'> Civic eye App </a> and sign up </P>";
            $message=$message."<P> 2. Click on # icon and type the report number on the Enter Report Number field then click Search </P>";
            $message=$message."<P> 3. Report details should appear on the screen </P><br/>";
            $message=$message."<P> Civic Eye app aims to help government combat corruption and malpractice by giving citizens the means to report their concerns directly to the mayor’s office. </P>";
            $message=$message."<P> If you have any concerns regarding this report, you may email the app administrators through ".   $ADMIN_MAIL. "</P>";
            $message=$message."<P> Best regards, Civic Eye Team  </P><br/>";
            
            
            if ($parm['anonymous_flag']==0)
                $message=$message."<span style='text-align:center' >Reporter:  ".$report_user['username']." , " . $report_user['email'] .' </span><br />';

            $mail_header="MIME-Version: 1.0 ";
            $mail_header .= "From: Civic Eye App User \r\n";
            $mail_header .= "Content-type: text/html;charset=utf-8 ";
            $mail_header .= "X-Priority: 3";
            $mail_header .= "X-Mailer: smail-PHP ".phpversion();

            $report = $this->Report_model->get_report($report_insert_id);
            
           if ($parm['receive_email']==1){

//               $retval = mail($mailto, $subject, $message, $mail_header);

               $retval= sendmailbymailgun($mailto, "Civic Eye App User", get_adminmail(), "Civic Eye Support", $subject, $message );
       //        $retval1= mail($mailto, $subject, $message, $mail_header);
                
                if( $retval == "true" ||  $retval1==true ) {
            
                    // $data['report_id']=$report_insert_id;
                  
                    $data['status']='OK';
                    $data['msg']='Success Send your Report on Email';
                    $data['report']= $report;

                }else {
                    $data['status']='Error';
                    $data['msg']='You failed to send your report on Email';
                }
            }

          
            if ($parm['receive_fax']==1){

                if ($this->make_pdf($report_insert_id,$APP_URL)=="true"){

                        /// if fax text ok, remove comment.                   
                   /*
                     $media=$this->config->base_url()."tmp/report_".$report_insert_id.".pdf";

                        if (send_twiliofax($faxto,$media)=="true"){
                            $data['status']='OK';
                            $data['msg']=$data['msg'].' : Success Send your Report on Fax';
                            $data['report']= $report;
        
                        }else {
                            $data['status']='Error';
                            $data['msg']='You failed to send your report on fax';
                        }
                        */
                }
            }

        }else{
            $data['status']='Input error';
            $data['msg']='Input your Report Information';
        }
        echo json_encode($data);
    }

    public function make_pdf($report_id,$app_url){

        $data['report_id']=$report_id;
        $data['app_url']=$app_url;
        $data['admin_mail']=get_adminmail();   

        $filename="report_".$report_id.".pdf";
        $msg= $this->load->view('reports/report_pdf', $data,true);
        $html_content = mb_convert_encoding($msg, 'HTML-ENTITIES', 'UTF-8');
        $this->pdf->loadHtml($html_content);
        $this->pdf->setPaper('B5','Portrait');
        $this->pdf->render();
       // $this->pdf->stream("report".$report_id.".pdf", array("Attachment"=>0));

         $pdf_string = $this->pdf->output();

         if (file_put_contents('tmp/'.$filename, $pdf_string) != false) 
            return "true";
        else
            return "false";
    }
    
    public function get_report(){
        $report_id=$this->input->post('report_id');
        if (!empty($report_id)){
            $report = $this->Report_model->get_report($report_id);

            if ($report){
                $data['status']='OK';
                $data['report']= $report;

            }else{
                $data['status']='Error';
                $data['msg']='Invalid your report number.';
           }
        }else{
            $data['status']='Input error';
            $data['msg']='Input your report_id!';
        }
        echo json_encode($data);
    }

    public function get_all_reportid(){

        $report_ids = $this->Report_model->get_all_reportid();

        if ($report_ids){
            $data['status']='OK';
            $data['report_ids']= $report_ids;
        }else{
            $data['status']='Error';
            $data['msg']='You failed to get all report ids.';
        }
        echo json_encode($data);
    }


    public function send_abuse(){
        $userid=$this->input->post('userid');
        $report_id=$this->input->post('report_id');
        $comment=$this->input->post('comment');

        if (!empty($userid) && !empty($report_id) && !empty($comment) ){
            $abuse_report = $this->Report_model->save_abuse($userid,$report_id,$comment);

            if ($abuse_report){
                    $data['status']='OK';
                    $data['msg']='You sent abuse report successfully.';
                
            }else{
                $data['status']='Error';
                $data['msg']='Abuse report sending failed';
            }

        }else{
            $data['status']='Input error';
            $data['msg']='Input your abuse report information';
        }
        echo json_encode($data);
    }

    public function user_abuse_confirm(){
        $userid=$this->input->post('userid');
        $report_id=$this->input->post('report_id');

         if (!empty($userid) && !empty($report_id) ){
            $abuse_report = $this->Report_model->get_user_abuse($userid,$report_id);

            if ($abuse_report){
                    $data['status']='OK';
                    $data['msg']='You had already sent abuse about this report';
                    $data['abuse_id']= $abuse_report['id'];
            }else{
                $data['status']='Error';
                $data['msg']='You have no abuse about this report.';
            }

        }else{
            $data['status']='Input error';
            $data['msg']='Input your report_id and userid';
        }
        echo json_encode($data);
    }
    
    public function get_all_citydata(){
   
        $city_list = $this->Report_model->get_all_citydata();

        if ($city_list){
                $data['status']='OK';
                $data['city_list']= $city_list;
        }else{
            $data['status']='Error';
            $data['msg']='You are failed to get City_data';
        }
        echo json_encode($data);
    }

    public function get_cityinfo(){
        $city_id=$this->input->post('city_id');
        if (!empty($city_id) ){

            $city_info = $this->Report_model->get_cityinfo($city_id);
            if ($city_info){
                    $data['status']='OK';
                    $data['city_info']= $city_info;
            }else{
                $data['status']='Error';
                $data['msg']='You failed to get City Fax,mail Information';
            }
        }
        else{
            $data['status']='Input error';
            $data['msg']='Input a city_id';
        }
            echo json_encode($data);
    }

    public function send_request_city(){
        $city_id=$this->input->post('city_id');
        $userid=$this->input->post('userid');
        $fax_req=$this->input->post('fax_req');
        $email_req=$this->input->post('email_req');

        if (!empty($userid) && !empty($city_id) && (!empty($fax_req) || !empty($email_req)) ){

            $send_cityreq = $this->Report_model->send_cityrequest($userid,$city_id,$fax_req,$email_req);

            if ($send_cityreq){
                    $data['status']='OK';
                    $data['msg']='Send your city request to Civic eye admin. Please wait email repsonse from admin.';
                
            }else{
                $data['status']='Error';
                $data['msg']='You failed to send city request';
            }

        }else{
            $data['status']='Input error';
            $data['msg']='Input your city request information';
        }
        echo json_encode($data);
    }


    public function get_all_basedata() {

        $userid=$this->input->post('userid');
        if (!empty($userid)){
            $user = $this->User_model->get_userprofile($userid);
            $report= $this->Report_model->get_user_report($userid);
            $city_list = $this->Report_model->get_all_citydata();

            if ($user){
                $data['status']='OK';
                $data['msg']='Getting all base_data Successfully.';
                $data['username']=$user['username'];
                $data['gender']=$user['gender'];
                $data['age']=$user['age'];
                $data['avatar']=$user['avatar'];
                $data['reported_reports']=$report;
                $data['city_list']= $city_list;

            }else{
                $data['status']='Error';
                $data['msg']='You failed to get all base data.';
     
            }
        }else{
            $data['status']='Input error';
            $data['msg']='Invalid your userid!';
        }

        echo json_encode($data);
    
    }

}