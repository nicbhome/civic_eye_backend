<?php

defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url', 'html'));
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->model('User_model');
        $this->load->model('Report_model');

    }

    public function index()
    {
        if ($this->session->userdata('is_user_login')) {
            $data['title'] = 'ALL USERS';  
            $data['page_id']="2";
            $this->load->view('Layout/header', $data);
            $this->load->view('user/index', $data);
            $this->load->view('Layout/footer', $data);
        }else{
            header('Location:login');
        }
    }

    public function banned_users()
    {
        if ($this->session->userdata('is_user_login')) {
            $data['title'] = 'BANNED USERS';  
            $data['page_id']="3";
            $this->load->view('Layout/header', $data);
            $this->load->view('user/banned', $data);
            $this->load->view('Layout/footer', $data);

        }else{
            header('Location:login');
        }
    }
    
    public function profile()
    {
        $userid = $this->input->get('id');

        if ($this->session->userdata('is_user_login')) {
            $data['title'] = 'USER PROFILE';  
            $data['page_id']="-1";
            $data['user_profile'] = $this->User_model->get_userprofile($userid);    
            $data['user_reports'] = $this->Report_model->get_user_report($userid);    

            $this->load->view('Layout/header', $data);
            $this->load->view('user/profile', $data);
            $this->load->view('Layout/footer', $data);

        }else{
            header('Location:login');
        }
    }



    public function ban_user()
    {
        $userid=$this->input->post('userid');
        if (!empty($userid)){
            $ban = $this->User_model->update_ban_flag($userid,1);
            if ($ban) 
                echo "success";
            else    
                echo "fail";
        }else{
            echo "fail";
        }
    }

    public function lift_user()
    {
        $userid=$this->input->post('userid');
        if (!empty($userid)){
            $lift = $this->User_model->update_ban_flag($userid,0);
            if ($lift) 
                echo "success";
            else    
                echo "fail";
        }else{
            echo "fail";
        }
    }


    public function load_users(){

        $search_type=$this->input->post('search_type');
        $search_name=$this->input->post('search_name');
        $banned_flag=$this->input->post('banned_flag');

        if ($search_type==0)
            $user_list = $this->User_model->get_userlist($banned_flag);    
        elseif  ($search_type==1)
            $user_list = $this->User_model->search_user($search_name,$banned_flag);

        $data="";

      if ($user_list){
        foreach ($user_list as $user) {
            $data=$data."<div class='user_item '>
                          <div class='user_item_avatar '>";
            $data=$data."<a style='margin:auto' href='".$this->config->base_url()."user/profile?id=".$user['id']."'>";

           if($user['avatar']!=null && $user['avatar']!=''  )
               $data=$data."<img src='".$user['avatar']."' class='img-circle list_avatar'>";
            else
            $data=$data."<img src='".$this->config->base_url().'assets/images/nouser.png'. "' class='img-circle list_avatar'>";
            
            $data=$data. "</a></div> ";
            $data=$data. "<div class='user_info_div'><div class='user_item_name'>";

            if ($user['username']!='')
                $data=$data.$user['username']."  (".$user['email'].")</div>"   ;
            else
               $data=$data.$user['email']."</div>"   ;
    
            $data=$data. "<div class='user_item_link'> <a href='".$this->config->base_url().'user/profile?id=' .$user['id']."' class='user_link_text'> Go to Profile </a></div>  ";
            $data=$data. "</div></div>";
        }
      }else{
            $data=" <h2 class='text-center m-t-50'> There is no users.</h2>";

        }

        echo $data;
   }
      

 
    
}
