<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Reports extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url', 'html'));
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->model('Report_model');

    }

    public function index()
    {
        if ($this->session->userdata('is_user_login')) {
            $data['title'] = 'Reports';  
            $data['page_id']="0";
 
            $this->load->view('Layout/header', $data);
            $this->load->view('reports/index', $data);
            $this->load->view('Layout/footer', $data);

        }else{
            header('Location:login');
        }
    }
     
    public function report()
    {
        $reportid = $this->input->get('id');

        if ($this->session->userdata('is_user_login')) {
            $data['title'] = 'Report Detail';  
            $data['page_id']="-1";
            $data['report'] = $this->Report_model->get_report($reportid);    

            $this->load->view('Layout/header', $data);
            $this->load->view('reports/report', $data);
            $this->load->view('Layout/footer', $data);


        }else{
            header('Location:login');
        }
    }

    public function load_reports(){

        $reports = $this->Report_model->get_all_reports();   
        $data="";
 
       if ($reports){
         foreach ($reports as $report) {
             $data=$data." <div class='report_item '>
                             <div class='user_item_avatar' style='margin:auto' >";
             $data=$data."<a style='margin:auto' href='".$this->config->base_url()."user/profile?id=".$report['userid']."'>";
 
            if($report['avatar']!=null && $report['avatar']!=''  )
                $data=$data."<img src='".$report['avatar']."' class='img-circle list_avatar'>";
             else
                $data=$data."<img src='".$this->config->base_url().'assets/images/nouser.png'. "' class='img-circle list_avatar'>";
            
             $data=$data. "</a></div> ";
             $data=$data. "<div class='report_info'> <p style='margin-bottom:5px'>  <span class='report_label'> Reporter:</span> <span class='report_comment m-r-50'>";
             if ($report['username']!='')
                $data=$data.$report['username']."  (".$report['email'].")</span>"   ;
            else
                $data=$data.$report['email']."</span>"   ;

             $data=$data. "<span class='report_label'> Crime Type:</span> <span class='report_comment m-r-50'> ".$report['report_type'] ."</span><a href='". $this->config->base_url()."reports/report?id=" .$report['id']. "' class='report_link_text'> Report # " . $report['id'] ."</a></p> "; 
             $data=$data. "<p> <span class='report_label'> City:</span>  <span class='report_city  m-r-50'>".$report['location']." , ".$report['city']."</span>  <span class='report_label'> Report-Date:</span> <span class='report_comment'>".  date_format(date_create( $report['create_date']),'Y-m-d H:i')."</span> </p>";
             $data=$data. "</div></div>";
         }
       }else{
             $data=" <h2 class='text-center m-t-50'> There is no Reports.</h2>";
 
         }
 
         echo $data;
     }
       

     public function delete_report(){
        $report_id=$this->input->post('report_id');

        if ($this->session->userdata('is_user_login')) {
           if($this->Report_model->delete_report($report_id))
             echo 'success';
           else 
             echo 'error';
            
        }else{
            header('Location:login');
        }
     }
 
     
}
