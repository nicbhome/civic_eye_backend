<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Requests extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url', 'html'));
        $this->load->helper('mailgun');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->model('Report_model');

    }

    public function index()
    {
        if ($this->session->userdata('is_user_login')) {
            $data['title'] = 'FAX/EMAIL REQUESTS';  
            $data['page_id']="4";
            $data['requests']= $this->Report_model->get_all_cityRequest();   

            $this->load->view('Layout/header', $data);
            $this->load->view('fax_request/index', $data);
            $this->load->view('Layout/footer', $data);
        }else{
            header('Location:login');
        }
    }
    
    public function process_request(){
 
        $request_id=$this->input->post('request_id');
        $city_id=$this->input->post('city_id');
        $user_email=$this->input->post('user_email');
        $city_email=$this->input->post('city_email');
        $city_fax=$this->input->post('city_fax');
        $email_update=0;
        $fax_update=0;
        if ($city_email!='')
           $email_update=$this->Report_model->update_city($city_id,$city_email,'email');
      
        if ($city_fax!='')
           $fax_update=$this->Report_model->update_city($city_id,$city_fax,'fax');

         if ($email_update==1 || $fax_update==1)  {

                 $to=$user_email;

                 $subject="City Fax/Email Request Reply";
                 $message='';
                 $message=$message.'Your Requested City Fax/Email is following. <br /><br />';
                 if ($city_email!='')
                     $message=$message.' Email:'.$city_email.' <br />';
                 if ($city_fax!='')
                     $message=$message.' Fax:'.$city_fax.' <br />';

        
                $mail_header="MIME-Version: 1.0 ";
                $mail_header .= "From: Civic Eye Support \r\n";
                $mail_header .= "Content-type: text/html;charset=utf-8 ";
                $mail_header .= "X-Priority: 3";
                $mail_header .= "X-Mailer: smail-PHP ".phpversion();

          //      $retval = mail($to, $subject, $message, $mail_header);

                $retval= sendmailbymailgun($to, "Civic App User", get_adminmail(), "Civic Eye Support", $subject, $message );
                $retval1= mail($to, $subject, $message, $mail_header);
                
                if( $retval == "true" ||  $retval1==true ) {
                    $request_update=$this->Report_model->update_request($request_id,1);
                    echo 'success';
                }else {
                   echo "error";
                }
        }else{
            echo 'error';
        }

    }


    public function del_request(){
        $request_id = $this->input->get('id');

        if ($this->session->userdata('is_user_login')) {
            $this->Report_model->update_request($request_id,-1);

            $data['title'] = 'FAX REQUESTS';  
            $data['page_id']="4";
            $data['requests']= $this->Report_model->get_all_cityRequest();   

            $this->load->view('Layout/header', $data);
            $this->load->view('fax_request/index', $data);
            $this->load->view('Layout/footer', $data);

        }else{
            header('Location:login');
        }
    }

    public function load_requests(){

        $requests = $this->Report_model->get_all_cityRequest();   
        $data="";
 
       if ($requests){
         foreach ($requests as $request) {
             $data=$data."<div class='request_item m-b-20'>
                             <div class='request_item_avatar' style='margin:auto' >";

             $data=$data."<a style='margin:auto' href='".$this->config->base_url()."user/profile?id=".$request['userid']."'>";
 
            if($request['avatar']!=null && $request['avatar']!=''  )
                $data=$data."<img src='".$request['avatar']."' class='img-circle list_avatar'>";
             else
                $data=$data."<img src='".$this->config->base_url().'assets/images/nouser.png'. "' class='img-circle list_avatar'>";
            
             $data=$data. "</a></div> ";
             $data=$data. "<div class='x_panel request_content '>";
             $data=$data. "<div class='request_name'> <p style='width:99%'>" . $request['username']." reported  <span  class='request_city'>  ".$request['City']." ".$request['State']."</span>  is missing a Fax#   </p> ";
             $data=$data. " <a class='collapse-link fax_inputview_btn'> <i class='fa fa-chevron-up'></i> </a> </div>";
             $data=$data. "  <div class='x_content'>   <div class='request_input_area'>   <div class='request_input_fax'>";
             $data=$data. " <input class='form-control main_input' type='text' id='fax_num' placeholder='# Fax Number'> </div>";
             $data=$data. " <div class='request_fax_submitbtn'><button class='form-control search_btn'  id='submit'>Submit</button></div> ";
             $data=$data. "</div></div></div></div>";
         }
       }else{
             $data=" <h2 class='text-center m-t-50'> There is no Requests.</h2>";
 
         }
 
         echo $data;
     }
       
     
  
}
