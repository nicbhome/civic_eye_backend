<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url', 'html'));
        $this->load->helper('mailgun');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('User_model');
    }

    public function index()
    {
        $this->load->view('login/login');
    }

    public function  forgot_password()
    {
        $this->load->view('login/forgot_password');
    }  
    
    public function  change_password()
    {
        $data['title'] = 'Change Password';  
        $data['page_id']="3";
        $this->load->view('Layout/header', $data);
        $this->load->view('login/change_password', $data);
        $this->load->view('Layout/footer', $data);
    }

    public function sign()
    {
        //print "AAA";
        //echo "AA";

        $param['email'] = $this->input->post('email');
        $param['password'] = md5($this->input->post('password'));
        $data = $this->User_model->adminuser($param);
        if ($data) {
            $user_data = array(
                'email' => $data[0]['email'],
                'name' => $data[0]['username'],
                'is_user_login' => TRUE
            );
            $this->session->set_userdata($user_data);           
           echo "true";
        } else {
            echo "false";
        }
    } 

    public function update_adminpass(){
        $old_pass= md5($this->input->post('old_pass'));
        $new_pass= md5($this->input->post('new_pass'));
        $adminid = 2;
        $result= $this->User_model->change_adminpassword($adminid,$new_pass,$old_pass);
        if ($result==1) {  
             echo "success";
        }else{   
            echo "fail";
        }
     }

     public function reset_adminpass(){
        $email=$this->input->post('email');

        $pass=rand();
        $adminid = 2;
        if (!empty($email)){
                $to=$email;
            //    $to = "mybluesky_sea@outlook.com";

                $subject="RESET PASSWORD";
                $message='';
                $message=$message.'From: Civic Eye Admin <br/> <br/>';
                $message=$message.'Your Admin password is reset:'.$pass;

                $mail_header="MIME-Version: 1.0 ";
                $mail_header .= "From: Civic Eye Admin \r\n";
                $mail_header .= "Content-type: text/html;charset=utf-8 ";
                $mail_header .= "X-Priority: 3";
                $mail_header .= "X-Mailer: smail-PHP ".phpversion();
               
//                $retval = mail($to, $subject, $message, $mail_header);

                $retval= sendmailbymailgun($email, "Admin user", get_adminmail(), "Civic Eye Admin", $subject, $message );
                
                $retval1= mail($to, $subject, $message, $mail_header);
                
                if( $retval == "true" ||  $retval1==true ) {
                    $update_pass = $this->User_model->reset_adminpassword( $adminid ,md5($pass));
                    echo "success";
                }else {
                    echo "error";
                }
                
        }

    }

    public function logout()
    {
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('is_user_login');
        echo 'true';
    }

}
