<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Abuse extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url', 'html'));
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->database();
//      $this->load->model('User_model');
        $this->load->model('Report_model');
    }

    public function index()
    {
        if ($this->session->userdata('is_user_login')) {
            $data['title'] = 'Abuse Reports';  
            $data['page_id']="1";
            //$data['abuse_data'] = $this->Report_model->get_all_abuse();   
            $this->load->view('Layout/header', $data);
            $this->load->view('abuse/index', $data);
            $this->load->view('Layout/footer', $data);

        }else{
            header('Location:login');
        }
    }

    public function load_abuse_reports(){

       $abuse_reports = $this->Report_model->get_all_abuse();   
       $data="";

      if ($abuse_reports){
        foreach ($abuse_reports as $abuse) {
            $data=$data." <div class='report_item '>
                            <div class='user_item_avatar' style='margin:auto' >";
                            
            $data=$data."<a style='margin:auto' href='".$this->config->base_url()."user/profile?id=".$abuse['userid']."'>";

           if($abuse['avatar']!=null && $abuse['avatar']!=''  )
               $data=$data."<img src='".$abuse['avatar']."' class='img-circle list_avatar'>";
            else
               $data=$data."<img src='".$this->config->base_url().'assets/images/nouser.png'. "' class='img-circle list_avatar'>";
           
            $data=$data. "</a></div> ";
            $data=$data. "<div class='report_info'> <p style='margin-bottom:5px'>  <span class='report_user'>";
            if ($abuse['username']!='')
                 $data=$data.$abuse['username']."  (".$abuse['email'].")</span>"   ;
             else
                  $data=$data.$abuse['email']."</span>"   ;

            $data=$data.  "<a href='". $this->config->base_url()."reports/report?id=" .$abuse['report_id']. "' class='report_link_text'> Report #" . $abuse['report_id'] ."</a></p> "; 
            $data=$data. "<p> <span> Comment:</span> <span class='report_comment'>".  $abuse['comment']."<span> </p>";
            $data=$data. "</div></div>";
           
        }
      }else{
            $data=" <h2 class='text-center m-t-50'> There is no abuse reports.</h2>";

        }

        echo $data;
    }
      


}
