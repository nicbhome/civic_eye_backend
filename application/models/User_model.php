<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class User_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');

    }
  /////////////////////////////////// admin 
    public function adminuser($param)
    {
        $sql = "select * from admin where email='".$param['email']."' and `password`='".$param['password']."'";

        return $this->db->query($sql)->result_array();
    }

    public function change_adminpassword($adminid,$newpass,$oldpass){
        $data = array('password' => $newpass);
        $this->db->set($data);
        $this->db->where('id', $adminid);
        $this->db->where('password', $oldpass);
        $this->db->update('admin');
        return $this->db->affected_rows();

    }


    public function regCode($user_id, $code){

        $expired_date   = date( "Y-m-d H:i:s", strtotime( date( "Y-M-d H:i:s") ) + 24 * 3600 );
        $data           = array('expired_date' => $expired_date, 'user_id' => $user_id, 'code' => $code  );

        return $this->db->insert('restore_password_codes', $data);
    }

    public function reset_adminpassword($adminid,$pass){
        $data = array('password' => $pass);
        $this->db->set($data);
        $this->db->where('id', $adminid);
        $this->db->update('admin');
        return $this->db->affected_rows();
    }

  //////////////////////////////////////////////////////////////////////////   api 
    public function loginuser($email,$password){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('email', $email);
        $this->db->where('password', $password);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function registeruser($email,$password){
       // $avartar=$this->config->base_url().'uploads/useravatar/nouser.png';
        $data = array('email' => $email, 'password' => $password, 'username' => '', 'gender' => '','age' => '','avatar' => '','banned_flag' => '0','create_date' => date('Y-m-d H:i:s'));
        return $this->db->insert('user', $data);
    }

    public function get_all_useremail(){
        $this->db->select('id,email');
        $this->db->from('user');
        $query = $this->db->get();
        return $query->result_array();
    }
    
    public function confirm_email($email){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('email', $email);
        $query = $this->db->get();
        return $query->row_array();
    }


    public function change_password($userid,$newpass){
        $data = array('password' => $newpass);
        $this->db->set($data);
        $this->db->where('id', $userid);
        return $this->db->update('user');
    }

    public function reset_password($email,$password){
        $data = array('password' => $password);
        $this->db->set($data);
        $this->db->where('email', $email);
        return $this->db->update('user');
    }

    public function get_userprofile($userid){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('id', $userid);
        $query = $this->db->get();
        return $query->row_array();
    }


    public function  update_username($userid,$username){
        $data = array('username' => $username);
        $this->db->set($data);
        $this->db->where('id', $userid);
        return $this->db->update('user');
    }

    public function  update_gender_age($userid,$gender,$age){
        $data = array('gender' => $gender,'age' => $age );
        $this->db->set($data);
        $this->db->where('id', $userid);
        return $this->db->update('user');
    }

    public function  update_avatar($userid,$avatar){
        $data = array('avatar' => $avatar);
        $this->db->set($data);
        $this->db->where('id', $userid);
        $this->db->update('user');
        return $this->db->affected_rows();

    }


    /////////////////// admin 

    public function get_userlist($banned_flag){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('banned_flag', $banned_flag);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function search_user($search_name,$banned_flag){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->like('username', $search_name);
        $this->db->where('banned_flag', $banned_flag);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function  update_ban_flag($userid,$banned_flag){
        $data = array('banned_flag' => $banned_flag);
        $this->db->set($data);
        $this->db->where('id', $userid);
        return $this->db->update('user');
    }

}
