<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Report_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
    }
    /////////////////////// admin /////////////////////////
    public function get_all_abuse(){
        $this->db->select('abuse.*,username , email,avatar');
        $this->db->from('abuse');
        $this->db->join('user', 'abuse.userid = user.id');
        $this->db->where('delete_flag>=', 0);
        $this->db->order_by('create_date','desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_all_reports(){
        $this->db->select('report.*,username,email,avatar');
        $this->db->from('report');
        $this->db->join('user', 'report.userid = user.id');
        $this->db->where('delete_flag>=', 0);
        $this->db->order_by('create_date','desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function delete_report($report_id){
        $data = array('delete_flag' => '-1');
        $this->db->set($data);
        $this->db->where('id', $report_id);
        $this->db->update('report');
        return $this->db->affected_rows();
    }


    public function get_all_cityRequest(){
        $this->db->select('city_request.*,user.username,user.email as user_email,user.avatar,city.City, city.State');
        $this->db->from('city_request');
        $this->db->join('user', 'city_request.userid = user.id');
        $this->db->join('city', 'city_request.city_id = city.id');
        $this->db->where('process_flag>=', 0);
        $this->db->order_by('process_flag','asc');
        $this->db->order_by('create_date','desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function update_request($request_id,$process_flag){
        $data = array('process_flag' => $process_flag);
        $this->db->set($data);
        $this->db->where('id', $request_id);
        $this->db->update('city_request');
        return $this->db->affected_rows();
    }

    public function update_city ($city_id,$city_email_fax,$update_type){
        if ($update_type=='email')
            $data = array('Email' => $city_email_fax);

        if ($update_type=='fax')
            $data = array('Fax' => $city_email_fax);

        $this->db->set($data);
        $this->db->where('id', $city_id);
        $this->db->update('city');
        return $this->db->affected_rows();
    }

    



    //////////////////// api  //////////////////////////
    public function get_MaxReportid(){
        $sql = " SELECT MAX(id) AS report_id FROM report ";
      //  return $this->db->query($sql)->row_array();
        $row = $this->db->query($sql)->row_array();
        if ($row['report_id']>10000)
          return $row['report_id'];
        else
          return 10000;

    }
       
    public function save_report($param){
        $this->db->insert('report', $param);
        $param['id'] = $this->db->insert_id();
        return $param['id'];
    }

    public function get_report($report_id){
        $this->db->select('report.*,username,email,avatar');
        $this->db->from('report');
        $this->db->join('user', 'report.userid = user.id');
        $this->db->where('report.id', $report_id);
        $this->db->where('delete_flag>=', 0);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function get_user_report($userid){
        $this->db->select('*');
        $this->db->from('report');
        $this->db->where('userid', $userid);
        $this->db->where('delete_flag>=', 0);
        $this->db->order_by('create_date','desc');

        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_all_reportid(){
        $this->db->select('id');
        $this->db->from('report');
    //    $this->db->where('delete_flag>=', 0);

        $query = $this->db->get();
        $i=0;
        $reports_id=array();

        foreach ($query->result_array() as $row) {
                $reports_id[$i]= intval($row['id']);
                $i++;         
        }
        return $reports_id;
    }


    public function save_abuse($userid,$report_id,$comment){
        $data = array('userid' => $userid, 
                      'report_id' => $report_id, 
                      'comment' => $comment, 
                      'create_date' => date('Y-m-d H:i:s')
                     );
        return $this->db->insert('abuse', $data);
    }

    public function get_user_abuse($userid,$report_id){
        $this->db->select('*');
        $this->db->from('abuse');
        $this->db->where('userid', $userid);
        $this->db->where('report_id', $report_id);
        $query = $this->db->get();
        return $query->row_array();
    }


    public function get_all_citydata(){
        $this->db->select('id,City,State,Email,Fax,Title');
        $this->db->from('city');
        $query = $this->db->get();
        return $query->result_array();
    }
    
    public function get_cityinfo($city_id){
        $this->db->select('id,City,State,Email,Fax,Title');
        $this->db->from('city');
        $this->db->where('id', $city_id);
        $query = $this->db->get();
        return $query->row_array();
    }
    
    public function send_cityrequest($userid,$city_id,$fax_req,$email_req){
        $data = array('userid' => $userid, 
                      'city_id' => $city_id, 
                      'fax' => $fax_req, 
                      'email' => $email_req, 
                      'create_date' => date('Y-m-d H:i:s'),
                      'process_flag' => 0
                     );
        return $this->db->insert('city_request', $data);
    }
    
}
