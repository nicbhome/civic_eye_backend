<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
//- user_api
 $route['api/login']='App_api/login';
 $route['api/signup']='App_api/signup';
 $route['api/get_all_useremail']='App_api/get_all_useremail';
 $route['api/confirm_email']='App_api/confirm_email';
 $route['api/reset_password']='App_api/reset_password';
 $route['api/change_password']='App_api/change_password';
 $route['api/get_userprofile']='App_api/get_userprofile';
 $route['api/update_username']='App_api/update_username';
 $route['api/update_gender_age']='App_api/update_gender_age';
 $route['api/update_avatar']='App_api/update_avatar';
 //--- report_api
 $route['api/send_report']='App_api/send_report';
 $route['api/get_report']='App_api/get_report';
 $route['api/get_all_reportid']='App_api/get_all_reportid';
 $route['api/send_abuse']='App_api/send_abuse';
 $route['api/user_abuse_confirm']='App_api/user_abuse_confirm';
 $route['api/get_all_citydata']='App_api/get_all_citydata';
 $route['api/get_cityinfo']='App_api/get_cityinfo';
 $route['api/send_request_city']='App_api/send_request_city';
 $route['api/get_all_basedata']='App_api/get_all_basedata';
